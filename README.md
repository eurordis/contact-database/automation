# Automation for Contact Database

This is a N8N runner using pm2 for production ready deployment.

It aims to connect APIs, webhooks and other automations.
N8N was selected because it's easy to use, to view logs and to re-play.

Configuration is available in _.env_.

You shoud create a _.env.local_ file to overwrite the default config.

## Requirements

You should have Node 18 to run this project.
N8N is currently (dec 2023) not able to run on newer version.

```
npm install
```

## Running The Project

```
npm start
```

will start a process called _n8n_.

## Debugging

Checkout the logs with `npx pm2 logs`
